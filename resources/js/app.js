
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// date formating with moment js
var moment = require('moment');
moment().format();

//progress bar
import VueProgressBar from 'vue-progressbar'

import Popper from 'popper.js';
window.Popper = Popper;


//sweet alert
// const Swal = require('sweetalert2')
import Swal from 'sweetalert2'
window.Swal = Swal;

const toast = Swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: false,
  timer: 2000
});

window.toast = toast;


window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform';

window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueRouter from 'vue-router'
Vue.use(VueRouter)


import Gate from './Gate';
Vue.prototype.$Gate = new Gate(window.user);

let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue').default },
    { path: '/profile', component: require('./components/Profile.vue').default },
    { path: '/users', component: require('./components/Users.vue').default },
    { path: '/developer', component: require('./components/Developer.vue').default },
    { path: '/home', component: require('./components/Profile.vue').default },
    { path: '/invoices', component: require('./components/Invoicing.vue').default },
    { path: '/trash', component: require('./components/RecycleBin.vue').default },
    { path: '*', component: require('./components/Error404.vue').default },
  ]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
  'example-component',
  require('./components/ExampleComponent.vue').default
);

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue').default
);

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
  'error404',
  require('./components/Error404.vue').default
);

Vue.component('pagination', require('laravel-vue-pagination'));
`declare module 'laravel-vue-pagination';`


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// using Vue filters for string manipulation
Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('formatDate', function (date) {
  if (!date) return ''
  return moment(date).format('LLLL')
  // return moment(date).startOf('hour').fromNow();  
})

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '3px'
})

window.Fire = new Vue();

const app = new Vue({
  el: '#app',
  router,
  data: {
      search: ''
  },
  methods: {
    findIt: _.debounce(() => {
        Fire.$emit('searchBox');
      }, 1000)
  },
});